package com.utsab.younginn;

import com.utsab.younginn.APiModel.ApiModel;
import com.utsab.younginn.APiModel.DetailModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Test on 10/26/2016.
 */

public interface RetrofitApi {

    @GET("posts/")
    Call<ArrayList<ApiModel>> getData(@Query("") String id);

    @GET("posts/{id}/comments")
    Call<ArrayList<DetailModel>> getDetails(@Path("id") String id);
}
