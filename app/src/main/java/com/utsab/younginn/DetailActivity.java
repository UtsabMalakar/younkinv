package com.utsab.younginn;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.utsab.younginn.APiModel.DetailModel;
import com.utsab.younginn.Adapters.DetailAdapter;
import com.utsab.younginn.RealmDbHelper.ListDataBase;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailActivity extends AppCompatActivity {

    private ListView lv_details;
    private String id;
    private ArrayList<DetailModel> apiModelArrayList;
    private ListDataBase listDataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        lv_details = (ListView) findViewById(R.id.lv_details);
        listDataBase = new ListDataBase(this);

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");

        apiModelArrayList = new ArrayList<>();

        getDetails();




    }

    public void getDetails() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitApi retrofitApi = retrofit.create(RetrofitApi.class);

        Call<ArrayList<DetailModel>> getDetails = retrofitApi.getDetails(id);
        getDetails.enqueue(new Callback<ArrayList<DetailModel>>() {
            @Override
            public void onResponse(Call<ArrayList<DetailModel>> call, Response<ArrayList<DetailModel>> response) {
                for (int i = 0;i<response.body().size();i++){
                    apiModelArrayList.add(response.body().get(i));
                }
                setListItem();
            }

            @Override
            public void onFailure(Call<ArrayList<DetailModel>> call, Throwable t) {

            }
        });
    }

    private void setListItem() {
        lv_details.setAdapter(new DetailAdapter(this,apiModelArrayList));
        listDataBase.SaveIntoDatabaseDetails(apiModelArrayList);
    }
}
