package com.utsab.younginn.RealmDbHelper;

import android.content.Context;
import android.util.Log;

import com.utsab.younginn.APiModel.ApiModel;
import com.utsab.younginn.APiModel.DetailModel;
import com.utsab.younginn.RealmDBModel.ListRealmModel;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Test on 11/7/2016.
 */

public class ListDataBase {

    private Context context;
    private Realm realm;
    private ArrayList<DetailModel> apiDetailmodel;
    private ArrayList<ApiModel> apimodel;

    public ListDataBase(Context context){

        this.context = context;
        if (realm==null)
        realm = Realm.getDefaultInstance();
    }

    public void SaveIntoDatabaseList(ArrayList<ApiModel> apiModels){
        apimodel = apiModels;

        for (ApiModel apiModel:apimodel) {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(apiModel);
            realm.commitTransaction();
        }
    }

    public void SaveIntoDatabaseDetails(ArrayList<DetailModel> apiModels){
        apiDetailmodel = apiModels;

        for (DetailModel apiModel:apiDetailmodel) {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(apiModel);
            realm.commitTransaction();
        }
    }

    public void getDetails() {
        RealmResults<ListRealmModel> listRealmModels = realm.where(ListRealmModel.class)
                .findAll();
        String output = listRealmModels.get(0).toString();
        Log.d("FromDatabase: ",output);
    }
}

