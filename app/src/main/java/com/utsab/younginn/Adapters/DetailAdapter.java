package com.utsab.younginn.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.utsab.younginn.APiModel.DetailModel;
import com.utsab.younginn.R;

import java.util.ArrayList;

/**
 * Created by Test on 10/26/2016.
 */

public class DetailAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<DetailModel> apiModelArrayList;

    public DetailAdapter(Context context, ArrayList resource) {
        super(context, R.layout.list_item_details,resource);
        this.context = context;
        this.apiModelArrayList = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_details, parent, false);
        TextView tv_name = (TextView) rowView.findViewById(R.id.tv_name_details);
        TextView tv_id = (TextView) rowView.findViewById(R.id.tv_id_details);
        TextView tv_email = (TextView) rowView.findViewById(R.id.tv_email_details);
        TextView tv_body = (TextView) rowView.findViewById(R.id.tv_body_details);

        tv_name.setText("Name: "+apiModelArrayList.get(position).name);
        tv_id.setText((position+1)+" .");
        tv_email.setText("Email: "+apiModelArrayList.get(position).email);
        tv_body.setText(apiModelArrayList.get(position).body);


        return rowView;

    }

}
