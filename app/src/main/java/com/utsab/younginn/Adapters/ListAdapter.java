package com.utsab.younginn.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.utsab.younginn.APiModel.ApiModel;
import com.utsab.younginn.R;

import java.util.ArrayList;

/**
 * Created by Test on 10/26/2016.
 */

public class ListAdapter extends ArrayAdapter<ArrayList> {

    private Context context;
    private ArrayList<ApiModel> apiModelArrayList;

    public ListAdapter(Context context, ArrayList resource) {
        super(context, R.layout.list_item_layout,resource);
        this.context = context;
        this.apiModelArrayList = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_layout, parent, false);
        TextView tv_lists = (TextView) rowView.findViewById(R.id.tv_title);
        TextView tv_id = (TextView) rowView.findViewById(R.id.name_id);

        tv_lists.setText(apiModelArrayList.get(position).title);
        tv_id.setText(apiModelArrayList.get(position).id+".");


        return rowView;

    }

//    @Override
//    public int getCount() {
//        return super.getCount();
//    }
}
