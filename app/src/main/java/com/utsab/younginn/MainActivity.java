package com.utsab.younginn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.utsab.younginn.APiModel.ApiModel;
import com.utsab.younginn.Adapters.ListAdapter;
import com.utsab.younginn.RealmDbHelper.ListDataBase;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private ListView lv_listings;
    private ArrayList<ApiModel> apiModelArrayList;
    private ListDataBase listDataBase;

    private static final String TAG = "TestVal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv_listings = (ListView) findViewById(R.id.lv_listings);
        listDataBase = new ListDataBase(this);
        apiModelArrayList = new ArrayList<>();

        getListings();

        lv_listings.setOnItemClickListener(this);
    }

    private void getListings() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitApi retrofitApi = retrofit.create(RetrofitApi.class);
        Call<ArrayList<ApiModel>> fetchData = retrofitApi.getData("");

        fetchData.enqueue(new Callback<ArrayList<ApiModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ApiModel>> call, Response<ArrayList<ApiModel>> response) {

                for (int i = 0;i<response.body().size();i++){
                    apiModelArrayList.add(response.body().get(i));
                }
                setListItem();


            }

            @Override
            public void onFailure(Call<ArrayList<ApiModel>> call, Throwable t) {

            }
        });
    }

    private void setListItem() {
        if (apiModelArrayList.size()>0){
            this.lv_listings.setAdapter(new ListAdapter(this,apiModelArrayList));
        }
        listDataBase.SaveIntoDatabaseList(apiModelArrayList);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent openerDetails = new Intent(this,DetailActivity.class);
        String position = String.valueOf(i+1);
        openerDetails.putExtra("id",position);
        startActivity(openerDetails);
    }
}
